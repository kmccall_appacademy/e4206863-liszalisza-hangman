class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  def setup
    secret_length = referee.pick_secret_word
    guesser.register_secret_length(secret_length)
    @board = ("_" * secret_length).chars
  end

  def take_turn
    letter = guesser.guess(board)
    indices = referee.check_guess(letter)
    update_board(indices, letter)
    puts board.join
    guesser.handle_response(letter, indices)
  end

  def update_board(indices, letter)
    return board if indices.empty?
    indices.each do |i|
      board[i] = letter
    end
  end

  def over?
    referee.secret_word == board.join
  end

  def play
    setup
    num_guesses = 0
    puts board.join
    until over?
      take_turn
      num_guesses += 1
    end
    "Congrats, you completed the word! #{num_guesses} guesses"
  end
end

class HumanPlayer
  attr_reader :secret_word

  def pick_secret_word
    print "Enter the length of your secret word: "
    @secret_word = $stdin.gets.chomp
    @secret_length = @secret_word.length
  end

  def register_secret_length(secret_length)
    puts "Find a word of length #{secret_length}"
  end

  def guess(board)
    print "Guess a letter: "
    letter = $stdin.gets.chomp.downcase
  end

  def check_guess(letter)
    # Returns array of indices of where the letter occurs
    (0...@secret_word.length).select do |i|
      @secret_word[i] == letter
    end
  end

  def handle_response(letter, indices)
    if indices.empty?
      puts "Your letter wasn't found in the word"
    end
  end
end

class ComputerPlayer
  attr_reader :secret_word
  attr_accessor :candidate_words

  def read_dictionary
    File.readlines("lib/dictionary.txt").map { |line| line.chomp }
  end

  def initialize(dictionary = read_dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(secret_length)
    puts "Find a word of length #{secret_length}"

    # Reject words from candidate_words that are the wrong length
    @candidate_words = @dictionary.select do |word|
      word.length == secret_length
    end
  end

  def guess(board)
    # Returns most common letter within remaining candidate_words,
    # minus letters already on the board
    str = @candidate_words.join
    all_letters = str.chars.sort_by { |char| str.count(char) }
    most_common = (all_letters - board).last
  end

  def check_guess(letter)
    # Returns array of indices of where the letter occurs
    (0...@secret_word.length).select do |i|
      @secret_word[i] == letter
    end
  end

  def handle_response(letter, indices)
    if indices.empty?
      puts "Your letter wasn't found in the word"
      @candidate_words = @candidate_words.reject do |word|
        word.include?(letter)
      end
    else # if letters are found and indices aren't nil
      # select candidate_words that have the letter in the right spot
      # (and nowhere else)
      @candidate_words = @candidate_words.select do |word|
        indices.all? { |i| word[i] == letter } &&
        ((0...word.length).to_a - indices).none? { |i| word[i] == letter }
      end
    end
  end
end
